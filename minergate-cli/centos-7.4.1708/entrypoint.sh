#!/bin/bash
echo ""
echo "*********************************"
echo "Mining on Docker! (Docker-Based MinerGate-Cli Beta 1.0)"
echo ""
echo "Powered By iLemonrain (ilemonrain@ilemonrain.com)"
echo "Docker Image: ilemonrain/minergate-cli:centos7"
echo "Project: https://hub.docker.com/r/ilemonrain/minergate-cli/"
echo "*********************************"
echo ""
sleep 1
echo "[Info] Detected $(cat /proc/cpuinfo| grep "processor"| wc -l) core(s)"
echo "[Info] Preparing the mining enviroment ..."
echo "[Info] Applying Host Patches For xmr.pool.minergate.com ..."
sleep 1
echo "94.130.64.225 xmr.pool.minergate.com" >> /etc/hosts
echo "136.243.102.157 xmr.pool.minergate.com" >> /etc/hosts
echo "94.130.9.194 xmr.pool.minergate.com" >> /etc/hosts
echo "46.4.120.155 xmr.pool.minergate.com" >> /etc/hosts
echo "176.9.0.89 xmr.pool.minergate.com" >> /etc/hosts
echo "94.130.48.154 xmr.pool.minergate.com" >> /etc/hosts
echo "76.9.47.243 xmr.pool.minergate.com" >> /etc/hosts
echo "176.9.147.178 xmr.pool.minergate.com" >> /etc/hosts
echo "136.243.88.145 xmr.pool.minergate.com" >> /etc/hosts
echo "78.46.23.253 xmr.pool.minergate.com" >> /etc/hosts
echo "136.243.94.27 xmr.pool.minergate.com" >> /etc/hosts
echo "136.243.102.167 minergate.com" >> /etc/hosts
sleep 1
echo "[Info] Applying Host Patches For minergate.com ..."
echo "136.243.102.154 minergate.com" >> /etc/hosts
echo "136.243.102.167 minergate.com" >> /etc/hosts
echo "88.99.142.163 minergate.com" >> /etc/hosts
echo "94.130.143.162" >> /etc/hosts
echo "[Info] Host Patch Success !"
sleep 1
echo "[Info] Starting the Miner ..."
echo "[Info] Using MinerGate Account : $user"
echo "[Info] Using Parameter : $parameter"
minergate-cli -u $user $parameter
echo "[Info] Mining Stopped !"
echo "[Info] Docker Contianer will stop soon !"
