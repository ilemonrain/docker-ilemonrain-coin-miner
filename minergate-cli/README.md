## Mining-on-Docker Project ! by iLemonrain  
   
### (Minergate-cli on Docker —— Docker**一键挖矿**企划)  
  
### 0. 简单说两句  
最近挖矿挖的有点走火入魔，同时为了发扬懒到家的精神，制作了这么一个Docker镜像，意在提供一个开包即用的挖矿环境，让复杂繁琐的挖矿配置变得非常简单，实现一键启动挖矿功能。  
  
企划目前仅仅是Beta阶段，仅实现了基础的功能，更多的功能，有待后续开发（有生之年系列）……
   
### 1. 镜像说明
镜像名称：ilemonrain/minergate-cli  
镜像别名：Mining-on-Docker Project ! (Minergate-cli on Docker)  
基础镜像：CentOS 7 (based on centos offical image)  
推荐运行内存：256MB (最低极限，对于比256MB还小的机器，我以后会有对应的解决方案)  
  
### 2. 如何使用镜像
启动命令行：  
docker run -d -e "user=ilemonrain@ilemonrain.com" -e "parameter=--xmr 1" --name="minergate-docker" ilemonrain/minergate-cli  
**-d**：后台运行，推荐带上这个参数，毕竟挖矿就是个无限挂机的过程……  
**-e "user=xxx"**：这里请填上你在Minergate官网的注册账号 （没有账号？>> [点这里注册一个](https://minergate.com/a/36a1dcff931a2db3eac0c351) <<）  
**-e "parameter=xxx"**：这里请填上你启动Minergate的参数，比如你要用2核心挖取XMR币，那么参数请填“--xmr 2”  
**--name="xxx"**：为容器命名（方便管理），最好留着，如果需要紧急终止挖矿容器的运行，使用 “docker kill minergate-docker” 即可快速终止  
**ilemonrain/minergate-cli**：镜像名称，一般情况下不需要动  
   
调取运行日志（用来看运行状态）：  
docker logs minergate-docker  
  
结束挖矿容器运行：  
docker kill minergate-docker  

### 3. 补充几句  
挖矿这个东西，原则上不建议在小鸡（VPS、云服务器等）上运行，会一定程度影响你的邻居的正常工作，独服请随意；万事稳为先，我会考虑在以后的版本中，加上控制占用率的相关组件，毕竟，目前还是一个测试，以后随缘开发……  
  
联系作者：E-Mail到ilemonrain@ilemonrain.com

支持&赞助：使用Minergate，向 ilemonrain@ilemonrain.com 账号挖矿（任何币种都可以）或者联系E-Mail获取其他渠道  
  
### 4. 更新日志  
2018/1/10：初始版本，开始Beta测试