#!/bin/bash
echo ""
echo "==============="
echo ""
echo "Starting XMRig-on-Docker ..."
echo ""
echo "Mining Power set to : $minerpower% "
echo "Mining Pool is : $minerpool "
echo "Mining User is : $mineruser "
echo "Mining Password is : $minerpassword "
echo ""
echo "==============="
echo ""
echo "Starting CPU Limiter with $minerpower% limit ..."
nohup cpulimit -e xmrig -l $minerpower &
echo "Starting Miner ..."
echo ""
xmrig -a cryptonight -o $minerpool -u $mineruser -p $minerpassword -k

