#!/bin/bash
echo ""
echo "==============="
echo ""
echo "Starting Minerd-on-Docker ..."
echo ""
echo "Mining Power set to : $minerpower% "
echo "Mining Pool is : $minerpool "
echo "Mining User is : $mineruser "
echo "Mining Thread is : $minerthread "
echo ""
echo "==============="
echo ""
echo "Applying Host Patches ..."
/bin/sh /hostpatch.sh
echo "Starting CPU Limiter with $minerpower% limit ..."
nohup cpulimit -e minerd -l $minerpower &
echo "Starting Miner ..."
echo ""
minerd -a cryptonight -o $minerpool -u $mineruser -p x -t $minerthread

